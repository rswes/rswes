from FB_LW_function import Calculate_SWE_CS_FB_LW,Calculate_SWE_CS_FB_LW_VT
import numpy as np
import math
import matplotlib.pyplot as plt


def calculate_KE(us, vs, hs, H):
    '''Returen kinetic energy as a funciton of time for 2d arrays of 2
        components of velocity, us an vs, height anomaly hs and mean height H 
       (H is a scalar)'''
    g = H
    KE = np.zeros(us.shape[0])
    i = 0
    hs1 = hs + H
    for u, v, h in zip(us, vs, hs1):  # velocity and height in a certain time
        KE[i] = 1 / 2 * (np.sum(h * u ** 2) + np.sum(h * v ** 2)) + 0.5 * g * np.sum(h ** 2)
        i += 1
    return KE


def calculate_M(hs):
    M = np.zeros(hs.shape[0])
    i = 0
    for h in hs:
        M[i] = np.sum(h)
        i += 1
    return M


def closest(mylist, Number):
    answer = []
    for i in mylist:
        answer.append(abs(Number - i))
    return answer.index(min(answer))


def calculate_APE(hs):
    g = 10
    APE = np.sum(0.5 * g * hs * hs)
    return APE



'exact solutions'
T = 10
e = 0.1
dx = 0.01 * math.pi
miu = 0.002
xs, h, u, v, g, H, dt, dx, ts = \
    Calculate_SWE_CS_FB_LW(e, 1 / 340, dx, T, 2 * math.pi, miu)
xs, h_exact, u_exact, v_exact, g, H, dt, dx, ts_exact = \
    Calculate_SWE_CS_FB_LW(e, 1 / 3400, dx, T, 2 * math.pi, miu)

x_length = int(2 * math.pi / dx) + 1

xs = np.zeros(x_length)
for i in range(0, xs.shape[0]):
    xs[i] = i * dx

'Plot RMSE_vs_Time'

RMSE = np.zeros(h.shape[0])

for n in range(RMSE.shape[0]):
    RMSE[n] = np.sqrt(np.mean(np.square(h[n] - h_exact[closest(ts_exact, ts[n])])))

plt.plot(ts, RMSE)
plt.title('The error growth over forecast duration T = ' + str(T))
plt.xlabel('Time')
plt.ylabel('RMSE')

plt.show()

'mass conservation'
# There are a lot of numbers hard coded
plt.subplot(121)
M = calculate_M(h + 10)
plt.plot(ts[0:-1], (M[0:-1] - M[0]) / M[0])
plt.xlabel('Time')
plt.ylabel('(M-M0)/M0')
plt.title('Mass conservation of dt = 1/340')

plt.subplot(122)
M_exact = calculate_M(h_exact + 10)
plt.plot(ts_exact[0:-1], (M_exact[0:-1] - M_exact[0]) / M_exact[0])
plt.xlabel('Time')
plt.ylabel('(M-M0)/M0')
plt.title('Mass conservation of dt = 1/3000')
plt.savefig("FB_LW_massConservation.svg")
plt.show()

'energy conservation'

'calculate APE'

APE = calculate_APE(h[0])

plt.subplot(121)
KE = calculate_KE(u, v, h, 10)
plt.plot(ts, (KE - KE[0]) / APE * (-100))
plt.title('Energy Conservation for dt = 1/340', fontsize=17)
plt.ylabel('Percentage Change in Energy (%)', fontsize=14)
plt.gca().invert_yaxis()
plt.xlabel('Time', fontsize=14)

plt.subplot(122)
KE_exact = calculate_KE(u_exact, v_exact, h_exact, 10)
plt.plot(ts_exact, (KE_exact - KE_exact[0]) / APE * (-100))
plt.title('Energy Conservation for dt = 1/3000', fontsize=17)
plt.ylabel('Percentage of Energy Lost (%)', fontsize=14)
plt.gca().invert_yaxis()
plt.xlabel('Time', fontsize=14)
plt.show()

'compare fixed and variable time steps'

k=2

def VT_and_FV(T, dt0,an):
    dx = 0.01 * math.pi
    'solution for VT'
    xs, h_VT, u1, v1, g, H, num_of_time, dx, ts_VT = Calculate_SWE_CS_FB_LW_VT(e, dt0, an, dx, T, 2 * math.pi,miu)
    KE_VT = calculate_KE(u1,v1,h_VT,H)

    'set dt for fixed scheme'

    dt = ts_VT[-1] / (ts_VT.shape[0] - 1)

    'solution for FV'
    xs, h_FV, u1, v1, g, H, dt, dx, ts_FV = Calculate_SWE_CS_FB_LW(e, dt, dx, ts_VT[-1], 2 * math.pi,miu)
    KE_VT = calculate_KE(u1, v1, h_FV,H)

    return h_VT, h_FV, ts_VT, ts_FV,H,u1,v1

h_VT_05, h_FV_05, ts_VT_05, ts_FV_05,H,u1,v1 = VT_and_FV(1.5, 1 / (680*k),k)
h_exact_VT_05, h_exact_FV_05, ts_exact_VT_05, ts_exact_FV_05,H,u1,v1 = VT_and_FV(1.5, 1 / (6800*k),k)

RMSE_FV_800_05 = np.zeros(h_FV_05.shape[0])
RMSE_VT_800_05 = np.zeros(h_VT_05.shape[0])

for n in range(RMSE_FV_800_05.shape[0]):
    RMSE_FV_800_05[n] = np.sqrt(np.mean(np.square(h_FV_05[n] - h_exact_FV_05[closest(ts_exact_FV_05, ts_FV_05[n])])))
    RMSE_VT_800_05[n] = np.sqrt(np.mean(np.square(h_VT_05[n] - h_exact_VT_05[closest(ts_exact_VT_05, ts_VT_05[n])])))



plt.subplot(221)
KE = calculate_KE(u, v, h, 10)
plt.plot(ts, (KE - KE[0]) / APE * (-100))
plt.title('Energy Conservation for dt = 1/340s', fontsize=17)
plt.ylabel('Percentage Change in Energy (%)', fontsize=14)
plt.gca().invert_yaxis()
plt.xlabel('Time/s', fontsize=14)

plt.subplot(222)
KE_exact = calculate_KE(u_exact, v_exact, h_exact, 10)
plt.plot(ts_exact, (KE_exact - KE_exact[0]) / APE * (-100))
plt.title('Energy Conservation for dt = 1/3000s', fontsize=17)
plt.ylabel('Percentage of Energy Lost (%)', fontsize=14)
plt.gca().invert_yaxis()
plt.xlabel('Time/s', fontsize=14)


plt.subplot(223)
plt.plot(ts, RMSE)
plt.title('The error growth over forecast duration T = 10s', fontsize=17)
plt.xlabel('Time/s',fontsize=14)
plt.ylabel('RMSE/m',fontsize=14)

plt.subplot(224)
plt.plot(ts_FV_05, RMSE_FV_800_05, label='fixed time step')
plt.plot(ts_VT_05, RMSE_VT_800_05, label='variable time step')
plt.title('The error growth of two schemes for T=1.5s',fontsize = 17)
plt.xlabel('Time/s',fontsize = 14)
plt.ylabel('RMSE/m',fontsize = 14)
plt.legend()
plt.show()


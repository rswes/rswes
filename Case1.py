from Function_for_SWE_2 import Calculate_SWE_Case1,Calculate_SWE_VT
import math
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation

dx = 0.01 * math.pi


def calculate_KE(us, vs, hs, H):
    g = H
    KE = np.zeros(us.shape[0])
    i = 0
    hs1 = hs + H
    for u, v, h in zip(us, vs, hs1):  # velocity and height in a certain time
        KE[i] = 1 / 2 * (np.sum(h * u ** 2) + np.sum(h * v ** 2)) + 0.5 * g * np.sum(h * h)
        i += 1
    return KE


def calculate_M(hs):
    M = np.zeros(hs.shape[0])
    i = 0
    for h in hs:
        M[i] = np.sum(h)
        i += 1
    return M


def calculate_APE(hs):
    g = 10
    APE = np.sum(0.5 * g * hs * hs)
    return APE


def closest(mylist, Number):
    answer = []
    for i in mylist:
        answer.append(abs(Number - i))
    return answer.index(min(answer))


def VT_and_FV(T, dt0,k):
    dx = 0.01 * math.pi
    'solution for VT'
    xs, h_VT, u1, v1, g, H, num_of_time, dx, ts_VT = Calculate_SWE_VT(0.1, dt0, k, dx, T, 2 * math.pi)
    KE_VT = calculate_KE(u1, v1, h_VT, H)

    'set dt'

    dt = ts_VT[-1] / (ts_VT.shape[0] - 1)

    'solution for FV'
    xs, h_FV, u1, v1, g, H, dt, dx, ts_FV = Calculate_SWE_Case1(0.1, dt, dx, ts_VT[-1], 2 * math.pi)
    KE_VT = calculate_KE(u1, v1, h_FV, H)

    return h_VT, h_FV, ts_VT, ts_FV, H, u1, v1


'exact solutions'

xs, h, u, v, g, H, dt, dx, ts = Calculate_SWE_Case1(0.1, 1 / 250, dx, 10, 2 * math.pi)
xs, h_exact, u_exact, v_exact, g, H, dt, dx, ts_exact = Calculate_SWE_Case1(0.1, 1 / 2500, dx, 10, 2 * math.pi)


x_length = int(2 * math.pi / dx) + 1

xs = np.zeros(x_length)
for i in range(0, xs.shape[0]):
    xs[i] = i * dx

'plot of solution at the beginning, middle and the end'
plt.subplot(131)
plt.plot(xs,h[0])
plt.title('Surface height at T=0',fontsize=16)
plt.xlabel('Time/s',fontsize=14)
plt.ylabel('h/m',fontsize=14)
plt.ylim(-1.1,1.1)

nt = int(h.shape[0])
nt_m = int(nt/2)
plt.subplot(132)
plt.plot(xs,h[nt_m])
plt.title('Surface height at T=5',fontsize=16)
plt.xlabel('Time/s',fontsize=14)
plt.ylabel('h/m',fontsize=14)
plt.ylim(-1.1,1.1)

plt.subplot(133)
plt.plot(xs,h[-1])
plt.title('Surface height at T=10',fontsize=16)
plt.xlabel('Time/s',fontsize=14)
plt.ylabel('h/m',fontsize=14)
plt.ylim(-1.1,1.1)

plt.show()

'mass conservation'

plt.subplot(121)
M = calculate_M(h + 10)
plt.plot(ts[0:-1], (M[0:-1] - M[0]) / M[0])
plt.xlabel('Time/s',fontsize=14)
plt.ylabel('(M-M0)/M0',fontsize=14)
plt.title('Mass conservation of dt = 1/250',fontsize=17)

plt.subplot(122)
M_exact = calculate_M(h_exact + 10)
plt.plot(ts_exact[0:-1], (M_exact[0:-1] - M_exact[0]) / M_exact[0])
plt.xlabel('Time/s',fontsize=14)
plt.ylabel('(M-M0)/M0',fontsize=14)
plt.title('Mass conservation of dt = 1/2500',fontsize=17)
plt.show()

'energy conservation'

'calculate APE'

APE = calculate_APE(h[0])

plt.subplot(121)
KE = calculate_KE(u, v, h, 10)
plt.plot(ts, (KE - KE[0]) / APE * 100)
plt.title('Energy Conservation for dt = 1/250', fontsize=17)
plt.ylabel('Percentage of Energy Lost (%)', fontsize=14)
plt.xlabel('Time/s', fontsize=14)

plt.subplot(122)
KE_exact = calculate_KE(u_exact, v_exact, h_exact, 10)
plt.plot(ts_exact, (KE_exact - KE_exact[0]) / APE * 100)
plt.title('Energy Conservation for dt = 1/2500', fontsize=17)
plt.ylabel('Percentage of Energy Lost (%)', fontsize=14)
plt.xlabel('Time/s', fontsize=14)
plt.show()

'compare the reference solutions of fixed and variable time-stepping'

h_exact_VT, h_exact_FV, ts_exact_VT, ts_exact_FV,H,u1,v1 = VT_and_FV(10, 1 / 4800,2)

plt.subplot(121)
plt.plot(ts_exact_VT[1:-1] - ts_exact_VT[0:-2],label = 'Variable')
plt.plot(ts_exact_FV[1:-1] - ts_exact_FV[0:-2],label = 'Fixed',linestyle='--')
plt.xlabel('Number of Time Steps',fontsize=14)
plt.ylabel('dt/s',fontsize=14)
plt.title('The time steps in two schemes',fontsize=17)
plt.legend()

plt.subplot(122)
plt.plot(xs,h_exact_VT[-1],label='Variable')
plt.plot(xs,h_exact_FV[-1],label='Fixed',linestyle='--')
plt.title('The reference solutions at the end of the forecast',fontsize=17)
plt.ylabel('h/m',fontsize=14)
plt.xlabel('Time/s',fontsize=14)
plt.legend()
plt.show()

'RMSE vs time'
RMSE = np.zeros(h.shape[0])

for n in range(RMSE.shape[0]):
    RMSE[n] = np.sqrt(np.mean(np.square(h[n] - h_exact[closest(ts_exact, ts[n])])))

plt.plot(ts, RMSE)
plt.title('The error growth over forecast duration T=10',fontsize=20)
plt.xlabel('Time/s',fontsize=18)
plt.ylabel('RMSE/m',fontsize=18)
plt.show()

'compare RMSE of fixed and variable time schemes'

dt_max = 1/240
k = 2

h_VT_1, h_FV_1, ts_VT_1, ts_FV_1,H,u1,v1 = VT_and_FV(0.3, dt_max/k,k)
h_exact_VT_1, h_exact_FV_1, ts_exact_VT_1, ts_exact_FV_1,H,u1,v1 = VT_and_FV(0.5, dt_max/k/10,k)

h_VT_2, h_FV_2, ts_VT_2, ts_FV_2,H,u1,v1 = VT_and_FV(0.5, dt_max/k,k)
h_exact_VT_2, h_exact_FV_2, ts_exact_VT_2, ts_exact_FV_2,H,u1,v1 = VT_and_FV(1, dt_max/k/10,k)

h_VT_3, h_FV_3, ts_VT_3, ts_FV_3,H,u1,v1 = VT_and_FV(0.8, dt_max/k,k)
h_exact_VT_3, h_exact_FV_3, ts_exact_VT_3, ts_exact_FV_3,H,u1,v1 = VT_and_FV(1.5, dt_max/k/10,k)

x_length = int(2 * math.pi / dx) + 1
xs = np.zeros(x_length)
for i in range(0, xs.shape[0]):
    xs[i] = i * dx

RMSE_FV_1 = np.zeros(h_FV_1.shape[0])
RMSE_VT_1 = np.zeros(h_VT_1.shape[0])
RMSE_FV_2 = np.zeros(h_FV_2.shape[0])
RMSE_VT_2 = np.zeros(h_VT_2.shape[0])
RMSE_FV_3 = np.zeros(h_FV_3.shape[0])
RMSE_VT_3 = np.zeros(h_VT_3.shape[0])

for n in range(RMSE_FV_1.shape[0]):
    RMSE_FV_1[n] = np.sqrt(np.mean(np.square(h_FV_1[n] - h_exact_FV_1[closest(ts_exact_FV_1, ts_FV_1[n])])))
    RMSE_VT_1[n] = np.sqrt(np.mean(np.square(h_VT_1[n] - h_exact_VT_1[closest(ts_exact_VT_1, ts_VT_1[n])])))

for n in range(RMSE_FV_2.shape[0]):
    RMSE_FV_2[n] = np.sqrt(np.mean(np.square(h_FV_2[n] - h_exact_FV_2[closest(ts_exact_FV_2, ts_FV_2[n])])))
    RMSE_VT_2[n] = np.sqrt(np.mean(np.square(h_VT_2[n] - h_exact_VT_2[closest(ts_exact_VT_2, ts_VT_2[n])])))

for n in range(RMSE_FV_3.shape[0]):
    RMSE_FV_3[n] = np.sqrt(np.mean(np.square(h_FV_3[n] - h_exact_FV_3[closest(ts_exact_FV_3, ts_FV_3[n])])))
    RMSE_VT_3[n] = np.sqrt(np.mean(np.square(h_VT_3[n] - h_exact_VT_3[closest(ts_exact_VT_3, ts_VT_3[n])])))


plt.subplot(131)
plt.plot(ts_FV_1, RMSE_FV_1, label='fixed time step')
plt.plot(ts_VT_1, RMSE_VT_1, label='variable time step')
plt.title('The error growth of two schemes for T=0.5',fontsize = 16)
plt.xlabel('Time',fontsize = 16)
plt.ylabel('RMSE',fontsize = 16)
plt.legend()

plt.subplot(132)
plt.plot(ts_FV_2, RMSE_FV_2, label='fixed time step')
plt.plot(ts_VT_2, RMSE_VT_2, label='variable time step')
plt.title('The error growth of two schemes for T=1.0',fontsize = 16)
plt.xlabel('Time',fontsize = 16)
plt.ylabel('RMSE',fontsize = 16)
plt.legend()

plt.subplot(133)
plt.plot(ts_FV_3, RMSE_FV_3, label='fixed time step')
plt.plot(ts_VT_3, RMSE_VT_3, label='variable time step')
plt.title('The error growth of two schemes for T=1.5',fontsize = 16)
plt.xlabel('Time',fontsize = 16)
plt.ylabel('RMSE',fontsize = 16)
plt.legend()
plt.show()

plt.plot(ts_VT_3[1:-1] - ts_VT_3[0:-2],label = 'dt for variable time stepping')
plt.plot(ts_FV_3[1:-1] - ts_FV_3[0:-2],label = 'dt for fixed time stepping')
plt.xlabel('Number of Time Steps',fontsize=14)
plt.ylabel('dt',fontsize=14)
plt.legend()

plt.show()





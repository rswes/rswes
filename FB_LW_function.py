"""
This is the code for forward backward method for the linear terms and
Lax-Wendfroff for advection. Advection and other terms are split
"""

import math
import numpy as np

def Calculate_SWE_CS_FB_LW(e, dt, dx, T, X, miu):

    C0 = 1 / 0.9295634723684806

    num_of_time = int(T/dt) + 1
    nx = int(X/dx)

    u = np.zeros((num_of_time, nx))
    v = np.zeros((num_of_time, nx))
    h = np.zeros((num_of_time, nx))
    ts = np.zeros(num_of_time)
    xs = np.zeros(nx)

    # Temporary variables after advection
    uT = np.zeros(nx)
    vT = np.zeros(nx)
    hT = np.zeros(nx)

    'complete the calculation of time and spatial grids'
    for n in range(num_of_time):
        ts[n] = n * dt

    for i in range(nx):
        xs[i] = i * dx

    'initial conditions'
    for i in range(nx):
        x = i * dx
        h[0, i] = C0*(math.exp(-4*((x - math.pi / 2)**2)) \
                  *math.sin(3*(x - math.pi/2)) \
                + math.exp(-2*((x - math.pi)**2))*math.sin(8*(x - math.pi)))

    # Loop over all time steps
    for n in range(num_of_time - 1):

        # Advection of u, v and h using Lax Wendroff
        for j in range(0, nx):
            # Advective Courant numbers in the x+ and x- sides
            c0 = 0.5*dt/dx*(u[n,j] + u[n,(j-1)%nx])
            c1 = 0.5*dt/dx*(u[n,j] + u[n,(j+1)%nx])
            # Fluxes for Lax-Wendroff advection
            u0 = 0.5*(1+c0)*u[n,(j-1)%nx] + 0.5*(1-c0)*u[n,j]
            u1 = 0.5*(1+c1)*u[n,j] + 0.5*(1-c1)*u[n,(j+1)%nx]
            v0 = 0.5*(1+c0)*v[n,(j-1)%nx] + 0.5*(1-c0)*v[n,j]
            v1 = 0.5*(1+c1)*v[n,j] + 0.5*(1-c1)*v[n,(j+1)%nx]
            h0 = 0.5*(1+c0)*h[n,(j-1)%nx] + 0.5*(1-c0)*h[n,j]
            h1 = 0.5*(1+c1)*h[n,j] + 0.5*(1-c1)*h[n,(j+1)%nx]

            # Advect u, v and h using u
            uT[j] = u[n,j] - (c1*u1 - c0*u0)
            vT[j] = v[n,j] - (c1*v1 - c0*v0)
            hT[j] = h[n,j] - (c1*h1 - c0*h0)

        # Update linear terms - u
        for j in range(0, nx):
            # Linear terms forward in time
            u[n+1,j] = uT[j] + dt/e*vT[j] \
                     - dt/e*(hT[(j+1)%nx] - hT[(j-1)%nx])/(2*dx) \
                 + dt*miu*(uT[(j+1)%nx] - 2*uT[j] + uT[(j-1)%nx])/dx**2

        # Update linear terms - v
        for j in range(0, nx):
            v[n+1,j] = vT[j] - dt/e*u[n+1,j] \
                + dt*miu/dx**2*(vT[(j+1)%nx] - 2*vT[j] + vT[(j-1)%nx])

        # Update linear terms - h
        for j in range(0, nx):
            h[n+1,j] = hT[j] \
                     - dt/(e*2*dx)*(u[n+1,(j+1)%nx] - u[n+1,(j-1)%nx])  \
                + dt*miu/dx**2*(hT[(j+1)%nx] - 2*hT[j] + hT[(j-1)%nx])


        print('Time ',(n+1)*dt,'Advective Courant number', np.max(u[n+1])*dt/dx)
        print('Courant number for gravity waves ',math.sqrt(1/e * np.max(h[n+1] + 1/e)) * dt / dx)
        print('Courant number',np.max(np.sqrt(1/e * h[n+1] + 1/e) + u[n+1]) * dt / dx)
        print('Diffusion time step number ', miu*dt/dx**2)

    return xs, h, u, v, 1/e, 1/e, dt, dx, ts


def Calculate_SWE_CS_FB_LW_VT(e, dt0, an, dx, T, X, miu):

    C0 = 1 / 0.9295634723684806

    'calculate the Amplification constant a'
    a = (T / dt0 - 1) / (T / dt0 - 2)

    num_of_time = int(math.log(an, a)) + 1
    nx = int(X/dx)

    u = np.zeros((num_of_time, nx))
    v = np.zeros((num_of_time, nx))
    h = np.zeros((num_of_time, nx))
    ts = np.zeros(num_of_time)
    xs = np.zeros(nx)

    # Temporary variables after advection
    uT = np.zeros(nx)
    vT = np.zeros(nx)
    hT = np.zeros(nx)

    'create an array to store the lengths of every time steps'
    dts = np.zeros(num_of_time - 1)
    for n in range(num_of_time - 1):
        dts[n] = dt0 * (a ** n)

    'complete the calculation of time and spatial grids'
    for n in range(num_of_time - 1):
        ts[n + 1] = ts[n] + dt0 * (a ** n)


    for i in range(nx):
        xs[i] = i * dx

    'initial conditions'
    for i in range(nx):
        x = i * dx
        h[0, i] = C0*(math.exp(-4*((x - math.pi / 2)**2)) \
                  *math.sin(3*(x - math.pi/2)) \
                + math.exp(-2*((x - math.pi)**2))*math.sin(8*(x - math.pi)))

    # Loop over all time steps
    for n in range(num_of_time - 1):
        dt = dts[n]
        # Advection of u, v and h using Lax Wendroff
        for j in range(0, nx):
            # Advective Courant numbers in the x+ and x- sides
            c0 = 0.5*dt/dx*(u[n,j] + u[n,(j-1)%nx])
            c1 = 0.5*dt/dx*(u[n,j] + u[n,(j+1)%nx])
            # Fluxes for Lax-Wendroff advection
            u0 = 0.5*(1+c0)*u[n,(j-1)%nx] + 0.5*(1-c0)*u[n,j]
            u1 = 0.5*(1+c1)*u[n,j] + 0.5*(1-c1)*u[n,(j+1)%nx]
            v0 = 0.5*(1+c0)*v[n,(j-1)%nx] + 0.5*(1-c0)*v[n,j]
            v1 = 0.5*(1+c1)*v[n,j] + 0.5*(1-c1)*v[n,(j+1)%nx]
            h0 = 0.5*(1+c0)*h[n,(j-1)%nx] + 0.5*(1-c0)*h[n,j]
            h1 = 0.5*(1+c1)*h[n,j] + 0.5*(1-c1)*h[n,(j+1)%nx]

            # Advect u, v and h using u
            uT[j] = u[n,j] - (c1*u1 - c0*u0)
            vT[j] = v[n,j] - (c1*v1 - c0*v0)
            hT[j] = h[n,j] - (c1*h1 - c0*h0)

        # Update linear terms - u
        for j in range(0, nx):
            # Linear terms forward in time
            u[n+1,j] = uT[j] + dt/e*vT[j] \
                     - dt/e*(hT[(j+1)%nx] - hT[(j-1)%nx])/(2*dx) \
                 + dt*miu*(uT[(j+1)%nx] - 2*uT[j] + uT[(j-1)%nx])/dx**2

        # Update linear terms - v
        for j in range(0, nx):
            v[n+1,j] = vT[j] - dt/e*u[n+1,j] \
                + dt*miu/dx**2*(vT[(j+1)%nx] - 2*vT[j] + vT[(j-1)%nx])

        # Update linear terms - h
        for j in range(0, nx):
            h[n+1,j] = hT[j] \
                     - dt/(e*2*dx)*(u[n+1,(j+1)%nx] - u[n+1,(j-1)%nx])  \
                + dt*miu/dx**2*(hT[(j+1)%nx] - 2*hT[j] + hT[(j-1)%nx])


        print('Time ',(n+1)*dt,'Advective Courant number', np.max(u[n+1])*dt/dx)
        print('Courant number for gravity waves ',math.sqrt(1/e * np.max(h[n+1] + 1/e)) * dt / dx)
        print('Courant number',np.max(np.sqrt(1/e * h[n+1] + 1/e) + u[n+1]) * dt / dx)
        print('Diffusion time step number ', miu*dt/dx**2)

    return xs, h, u, v, 1/e, 1/e, dt, dx, ts


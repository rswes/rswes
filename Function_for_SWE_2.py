import math
import numpy as np

def Calculate_SWE_Case1(e, dt, dx, T, X):  # case1 is finite volume

    C0 = 1 / 0.9295634723684806
    g = 10
    miu = 0.0001
    # miu = 0
    H = 1 / e

    num_of_time = int(T / dt) + 1
    nx = int(X / dx) + 1

    u = np.zeros((num_of_time, nx))
    v = np.zeros((num_of_time, nx))
    h = np.zeros((num_of_time, nx))
    ts = np.zeros(num_of_time)
    xs = np.zeros(nx)

    'complete the calculation of t and x'
    for n in range(num_of_time):
        ts[n] = n * dt

    for i in range(nx):
        xs[i] = i * dx

    'initial conditions'
    for i in range(nx):
        x = i * dx
        # x = (x - math.pi / 2) % (2 * math.pi)
        h[0, i] = C0 * (math.exp(-4 * ((x - math.pi / 2) ** 2)) * math.sin(3 * (x - math.pi / 2)) + math.exp(
            -2 * ((x - math.pi) ** 2)) * math.sin(8 * (x - math.pi)))

    for n in range(num_of_time - 1):

        for j in range(0, nx):
            if u[n, j] >= 0:

                u[n+1, j] = u[n, j] + dt*(v[n,j]-(h[n,(j+1)%nx]-h[n,(j-1)%nx])/(2*dx))/e + \
                                  dt*miu*(u[n,(j+1)%nx]-2*u[n,j]+u[n,(j-1)%nx])/(dx*dx) \
                                  - dt*(u[n,j]*(u[n,j]-u[n,(j-1)%nx]))/dx

            if u[n, j] < 0:
                u[n+1,j] = u[n,j] + dt*(v[n,j]-(h[n,(j+1)%nx]-h[n,(j-1)%nx])/(2*dx))/e + \
                                  dt*miu*(u[n,(j+1)%nx]-2*u[n,j]+u[n,(j-1)%nx])/(dx*dx) \
                                  - dt*(u[n,j]*(u[n,(j+1)%nx]-u[n,j]))/dx

        for j in range(0, nx):
            if u[n + 1, j] >= 0:

                v[n + 1, j] = v[n, j] - u[n + 1, j] * dt / e + dt * miu * (
                              v[n, (j + 1)%nx] - 2 * v[n, j] + v[n, (j - 1)%nx]) / (dx * dx)\
                              - dt * u[n + 1, j] * (v[n, j] - v[n, (j - 1)%nx]) / dx

            if u[n + 1, j] < 0:

                v[n + 1, j] = v[n, j] - u[n + 1, j] * dt / e + dt * miu * (
                        v[n, (j + 1) % nx] - 2 * v[n, j] + v[n, (j - 1) % nx]) / (dx * dx) \
                              - dt * u[n + 1, j] * (v[n, (j+1)%nx] - v[n, j]) / dx


        for j in range(0, nx):

            h[n + 1, j] = h[n, j] + dt * (-((u[n + 1, (j + 1)%nx] - u[n + 1, (j-1)%nx]) / (2 * dx * e)) + miu * (
                        h[n, (j + 1)%nx] - 2 * h[n, j] + h[n, (j-1)%nx]) / (dx * dx)) \
                        - dt * ((h[n, (j + 1)%nx] + h[n, j]) * (u[n + 1, (j + 1)%nx] + u[n + 1, j])\
                        - (h[n, j] + h[n, (j-1)%nx]) * (u[n + 1, j] + u[n + 1, (j - 1)%nx])) / (4 * dx)

        # print('2',math.sqrt(g * np.max(h[n + 1] + 10)) * dt / dx)
        print(np.max(np.sqrt(g * h[n + 1] + 10) + u[n + 1]) * dt / dx)
    return xs, h, u, v, g, H, dt,dx, ts

def Calculate_SWE_VT(e,dt0, an, dx, T, X):
    C0 = 1 / 0.9295634723684806
    g = 10
    miu = 0.0001
    # miu = 0
    H = 1 / e

    'calculate the Amplification constant a'

    # a = (dt0 * (an - 1)) / T + 1

    a = (T / dt0 - 1) / (T / dt0 - 2)

    'calculate number of time steps'

    num_of_time = int(math.log(an, a)) + 1  # number of point = 833, number of slots = 832
    nx = int(X / dx) + 1

    u = np.zeros((num_of_time, nx))
    v = np.zeros((num_of_time, nx))
    h = np.zeros((num_of_time, nx))
    ts = np.zeros(num_of_time)
    xs = np.zeros(nx)

    'create an array to store dts'
    dts = np.zeros(num_of_time - 1)
    for n in range(num_of_time - 1):
        dts[n] = dt0 * (a ** n)

    'complete the calculation of t and x'
    for n in range(num_of_time - 1):
        ts[n + 1] = ts[n] + dt0 * (a ** n)

    for i in range(nx):
        xs[i] = i * dx

    'initial conditions'
    for i in range(nx):
        x = i * dx
        # x = (x - math.pi / 2) % (2 * math.pi)
        h[0, i] = C0 * (math.exp(-4 * ((x - math.pi / 2) ** 2)) * math.sin(3 * (x - math.pi / 2)) + math.exp(
            -2 * ((x - math.pi) ** 2)) * math.sin(8 * (x - math.pi)))

    for n in range(num_of_time - 1):

        for j in range(0, nx):
            if u[n, j] >= 0:

                u[n+1, j] = u[n, j] + dts[n]*(v[n,j]-(h[n,(j+1)%nx]-h[n,(j-1)%nx])/(2*dx))/e + \
                                  dts[n]*miu*(u[n,(j+1)%nx]-2*u[n,j]+u[n,(j-1)%nx])/(dx*dx) \
                                  - dts[n]*(u[n,j]*(u[n,j]-u[n,(j-1)%nx]))/dx

            if u[n, j] < 0:
                u[n+1,j] = u[n,j] + dts[n]*(v[n,j]-(h[n,(j+1)%nx]-h[n,(j-1)%nx])/(2*dx))/e + \
                                  dts[n]*miu*(u[n,(j+1)%nx]-2*u[n,j]+u[n,(j-1)%nx])/(dx*dx) \
                                  - dts[n]*(u[n,j]*(u[n,(j+1)%nx]-u[n,j]))/dx

        for j in range(0, nx):
            if u[n + 1, j] >= 0:

                v[n + 1, j] = v[n, j] - u[n + 1, j] * dts[n] / e + dts[n] * miu * (
                              v[n, (j + 1)%nx] - 2 * v[n, j] + v[n, (j - 1)%nx]) / (dx * dx)\
                              - dts[n] * u[n + 1, j] * (v[n, j] - v[n, (j - 1)%nx]) / dx

            if u[n + 1, j] < 0:

                v[n + 1, j] = v[n, j] - u[n + 1, j] * dts[n] / e + dts[n] * miu * (
                        v[n, (j + 1) % nx] - 2 * v[n, j] + v[n, (j - 1) % nx]) / (dx * dx) \
                              - dts[n] * u[n + 1, j] * (v[n, (j+1)%nx] - v[n, j]) / dx


        for j in range(0, nx):

            h[n + 1, j] = h[n, j] + dts[n] * (-((u[n + 1, (j + 1)%nx] - u[n + 1, (j-1)%nx]) / (2 * dx * e)) + miu * (
                        h[n, (j + 1)%nx] - 2 * h[n, j] + h[n, (j-1)%nx]) / (dx * dx)) \
                        - dts[n] * ((h[n, (j + 1)%nx] + h[n, j]) * (u[n + 1, (j + 1)%nx] + u[n + 1, j])\
                        - (h[n, j] + h[n, (j-1)%nx]) * (u[n + 1, j] + u[n + 1, (j - 1)%nx])) / (4 * dx)

        # print('2',math.sqrt(g * np.max(h[n + 1] + 10)) * dt / dx)
        print(np.max(np.sqrt(g * h[n + 1] + 10) + u[n + 1]) * dts[n] / dx)
    return xs, h, u, v, g, H, num_of_time, dx, ts 


